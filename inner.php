<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <title></title>
  <meta name="description">
  <meta content="width=device-width" name="viewport">
  <link href="application.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
  <![endif]-->
</head>
<body><div class='line'>
  <div class='wrap'>
    <a class='special' href='http://gooddays.ru/'>
      <img src='i/special.png'>
    </a>
  </div>
  <div class='bg'></div>
</div>
<div class='wrap clearfix'>
  <h1><?php echo $data['label'] ?></h1>
  <section>
    <div class='slider'>
      <?php foreach ($data['slider'] as $value): ?>
      <div class='item'>
        <div class='bg'>
          <img alt='' src='i/<?php echo $value ?>'>
        </div>
        <div class='shadow'>
          <?php echo $data['slider-label'] ?>
        </div>
      </div>
      <?php endforeach ?>
    </div>
    <?php foreach ($data['left'] as $value): ?>
    <div class='elem'>
      <img alt='' src='i/<?php echo $value['image'] ?>'>
      <div class='data'>
        <h3>
          <?php if ($value['link'] == ''): ?>
            <?php echo $value['title'] ?>
          <?php else: ?>
            <a href='<?php echo $value['link'] ?>'><?php echo $value['title'] ?></a>
          <?php endif ?>
        </h3>
        <p><?php echo $value['text'] ?></p>
        <div class='links'>
          <?php echo $value['links'] ?>
        </div>
      </div>
    </div>
    <?php endforeach ?>
  </section>
  <aside>
    <?php foreach ($data['right'] as $value): ?>
    <div class='elem'>
      <div class='label'><?php echo $value['region'] ?></div>
      <div class='sublabel'>Гарантированные туры</div>
      <?php foreach ($value['tours'] as $key => $value): ?>
      <div class='tour'>
        <div class='price'>
          <div class='lab'>
            от <br />
            <span><?php echo $value['price'] ?></span>
          </div>
          <img alt='' src='i/<?php echo $value['image'] ?>'>
        </div>
        <div class='data'>
          <a href='<?php echo $value['link'] ?>'><?php echo $value['title'] ?></a>
          <p><?php echo $value['text'] ?></p>
          <p class='date'><?php echo $value['dates'] ?></p>
        </div>
      </div>
      <?php endforeach ?>
    </div>
    <?php endforeach ?>
    <?php echo $data['relink'] ?>
  </aside>
</div>
<footer>
  <div class='wrap'>
    <div class='left'><a href='http://jazztour.ru/'><img src='i/footer_01.png'></a>
    <a href='http://gooddays.ru/'><img src='i/footer_02.png'></a></div>
    <div class='right'>
      <div class="counter">
          <script type="text/javascript">
        document.write('<img src="http://luxup.ru/tr/4227/&r='+escape(document.referrer)+'&t='+(new Date()).getTime()+'" width="1" height="1"/>');
      </script>
      <script type="text/javascript"><!--
        document.write("<a href='http://www.liveinternet.ru/click' "+
        "target=_blank><img src='http://counter.yadro.ru/hit?t24.1;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";"+Math.random()+
        "' alt='' title='LiveInternet: показано число посетителей за"+
        " сегодня' "+
        "border='0' width='88' height='15' ><\/a>")
      //--></script>
       <!-- Yandex.Metrika counter --><div style="display:none;"><script type="text/javascript">(function(w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter11393131 = new Ya.Metrika({id:11393131, enableAll: true, webvisor:true}); } catch(e) { } }); })(window, "yandex_metrika_callbacks");</script></div><script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script><noscript><div><img src="//mc.yandex.ru/watch/11393131" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
      </div>
    </div>
  </div>
</footer>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
</body>
</html>